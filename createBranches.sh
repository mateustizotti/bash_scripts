#!/bin/bash
# Script to create n branches in a BB repo
# Mateus Tizotti 14/10/2020

function ProgressBar {

    let _progress=(${1}*100/${2}*100)/100
    let _done=(${_progress}*4)/10
    let _left=40-$_done

    _fill=$(printf "%${_done}s")
    _empty=$(printf "%${_left}s")

    printf "\rProgress : [${_fill// /▇}${_empty// /-}] ${_progress}%%"
}

echo " "
echo Number of branches to be created: $1
echo On repo: $2
echo " "
echo Creating branches...
i=1
while [ $i -le $1 ]
do
    ProgressBar $i $1
    {
    cd $2
    git branch branch$i
    git checkout branch$i
    touch file$i.txt
    git add .
    git commit -m "Created branch$i with file$i"
    git push origin branch$i
    git checkout master
    } &> /dev/null
    i=$(( i+1 ))
done
