#!/bin/bash
file="userlist.txt"
credential="mateustizotti:jKbWUKTAvtTHkRG4u4J4"
i=0
while read line || [ -n "$line" ]
do
	i=$((i+1))
	echo "Invitation number: $i"
	echo "Invitation sent to: $line"
	cmd=$(curl -s -w "\nHTTP Code:  %{http_code}\n" --user $credential -X PUT --header "Content-Type: application/json" "https://api.bitbucket.org/1.0/users/mateustizotti/invitations" --data '{"email": \"$line\","group_slug":"test"}')
	echo "Response : $cmd"
	echo ""
done < $file