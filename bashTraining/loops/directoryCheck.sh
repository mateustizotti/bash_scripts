#!/bin/bash
# Script for checking every entries in a repo;
RED='\033[0;31m'
YELLOW='\033[1;33m'
LIGHTBLUE='\033[1;34m'
NOCOLOR='\033[0m'
cd $1
subdecrories=$(ls)

for i in $subdecrories
do 
	if [[ -d $i ]]; then
		cd $i
		n=$(ls -l | wc -l | xargs)
		echo -e "${LIGHTBLUE}$i${NOCOLOR} - Is a ${RED}Directory${NOCOLOR} - Has $n files"
		cd ..
	else
		size=$(ls -l $i | awk '{print $5}')
		echo -e "${LIGHTBLUE}$i${NOCOLOR} - Is a ${YELLOW}File${NOCOLOR} - Size is $size bytes"
	fi
done