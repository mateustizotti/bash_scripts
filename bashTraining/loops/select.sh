#!/bin/bash
# Select Example

names='Mateus Thaiane Rafael Quit'

PS3="Select a name:"


select name in $names
do 
	if [[ $name == 'Quit' ]]; then
		break
	fi
	echo "Hello $name"
done

echo Bye