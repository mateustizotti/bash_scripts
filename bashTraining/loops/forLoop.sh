#!/bin/bash
# Basic for loop

names='Mateus Thaiane Rafael'

for name in $names
do
	echo $name
done

for value in {1..5}
do
	echo $value
done

echo All done