#!/bin/bash
# Script for printing numbers and telling if they are even or odd

for i in {1..10}
do 
	if (( $i % 2==0 ))
	then
		echo "$i is even!"
	else
	echo "$i is odd"
	fi
done

echo Bye