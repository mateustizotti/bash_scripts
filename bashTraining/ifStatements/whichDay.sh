#!/bin/bash
# Script for checking which day is today

DATE=$(date | awk '{print $1}')

case $DATE in
	'Mon' )
		clear
		echo "I hate mondays :/"
		;;
	'Tue' )
		clear
		echo "At least it is not monday ¯\_(ツ)_/¯"
		;;
	'Wed' )
		clear
		echo "Happy hump day!"
		;;
	'Thu' )
		clear
		echo "We are almost there!"
		;;
	'Fri' )
		clear
		echo "We did it boys! TGIF!"
		;;	
esac