#!/bin/bash
# Script to analyze a file

if [[ -e $1 ]]; then
	echo This file exists and

	if [[ -s $1 ]]; then
		echo It is not empty
	else
		echo It is empty
	fi

	if [[ -r $1 ]]; then
		echo You can read it
	else
		echo You cannot read it
	fi

	if [[ -w $1 ]]; then
		echo You can modify it
	else
		echo You cannot modify it
	fi

	if [[ -x $1 ]]; then
		echo You can execute it
	else
		echo You cannot run it
	fi
else
	echo This file does not exist!
fi

