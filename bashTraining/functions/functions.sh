#!/bin/bash
# Simple function examples

print_something() {
	echo "Hey I'm a function!"
}

print_value() {
	echo "I'm a function and I've recieved $1 as a parameter"
}

print_and_return() {
	echo "Hey I'm a function returning 1 as exit status"
	return 1
}

print_something
print_value 5
print_and_return
echo "The above function returned $?"


