#!/bin/bash
# Script to print out tomorrows date

day=$(date | awk '{print $3}')
month=$(date | awk '{print $2}')
year=$(date | awk '{print $6}')

((day++))

echo "Tomorrow will be: $month/$day/$year" 