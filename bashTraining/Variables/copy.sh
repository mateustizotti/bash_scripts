#!bin/bash
# Script to create a copy with date

if [[ $# == 1 ]]; then
	now=$(date +"%b%d-%Y-%H%M%S")
	FILE="$1"
	name="${FILE%.*}"
	ext="${FILE##*.}"

	cp -v $FILE $name-$now.$ext
else
	echo "You need to provide a file!"
	exit
fi
