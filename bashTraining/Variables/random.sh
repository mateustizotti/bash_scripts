#!bin/bash
# Script to generate a random word

path=/usr/share/dict

if [[ $# == 1 ]]; 
then
	echo "The word in position $RANDOM with $1 letters is:"
	cd $path
	grep "^[A-Za-z]\{$1\}$" words | head -$RANDOM | tail -1
else
	echo "The word in position $RANDOM is:"
	cd $path
	cat words | head -$RANDOM | tail -1
fi