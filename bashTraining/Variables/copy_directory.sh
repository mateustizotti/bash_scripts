#!bin/bash
# Script to create a copy with date for a whole directory

if [[ $# == 1 ]]; then
	cd $1

	for file in *; do
		now=$(date +"%b%d-%Y-%H%M%S")
		name="${file%.*}"
		ext="${file##*.}"
		mv -v "$file" "$name-$now.$ext"
	done
else
	echo "You need to provide a path!"
	exit
fi
