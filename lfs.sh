#!/bin/bash
file=""
credential="username:password"
i=0
while read line || [ -n "$line" ]
do 
	i=$((i+1))
	echo "File number : $i"
	echo "LFS Object ID (OID) : $line" 
	cmd=$(curl -s -w "\nHTTP Code:  %{http_code}\n" --user $credential -X DELETE "https://api.bitbucket.org/internal/repositories/mateustizotti/objectorientedprogramming/lfs/$line")
	echo "Response : $cmd"
	echo ""
done < $file
echo ""
echo "Total LFS Object ID in $file :" 
grep -c "" $file