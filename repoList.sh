#!/bin/bash
file="<path_to_file>"
credential="<username>:<password>"
i=0
while read line || [ -n "$line" ]
do 
	i=$((i+1))
	echo "Repository name : $line" 
	cmd=$(curl -s -w "\nHTTP Code:  %{http_code}\n" --user $credential -X GET "https://api.bitbucket.org/2.0/repositories/technisys/$line/default-reviewers?fields=values.display_name")
	echo "Reviewers : $cmd"
	echo ""
done < $file