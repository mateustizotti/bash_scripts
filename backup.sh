#!/bin/bash
#Script for backing up my repos
#Mateus T 30/07/2020

if [ $# != 1 ]
then
    echo Usage: A single argument which is the directory to backup
    exit
fi
if [ ! -d ~/dev/$1 ]
then
    echo 'The given directory does not seem to exist (possible typo?)'
    exit
fi
date=`date +%F`
 
if [ -d ~/Backups/$1_$date ]
then
    echo 'This project has already been backed up today, overwrite?'
    read answer
    if [ $answer != 'y' ]
    then
        exit
    fi
else
    mkdir ~/Backups/$1_$date
fi
cp -R ~/dev/$1 ~/Backups/$1_$date
echo Backup of $1 completed!
